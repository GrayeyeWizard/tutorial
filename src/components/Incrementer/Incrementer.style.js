import { FormGroup } from 'reactstrap';
import styled from 'styled-components';

export const Container = styled(FormGroup)`
    display: flex;
    flex-direction: row;
    .form-control {
        margin-left: 10px;
        margin-right: 10px;
        text-align: right;
    }
    .btn {
        border-radius: 100%;
        width: 38px;
        height: 38px;
    }
`;