import React, { useState, useEffect } from "react";
import { Button, Input } from "reactstrap";
import { Container } from "./Incrementer.style";

const Incrementer = ({value, onChange}) => {
  const [data, setData] = useState(0);

  const initData = () => {
    setData(value);
  }

  useEffect(() => {
    console.log('didMount');
    initData();
    return () => {
        console.log('willUnmount');
    }
  }, []);

  const increase = () => {
    const value = data + 1;
    setData(value);
    onChange(value);
  };

  const decrease = () => {
    if (data > 0) {
      const value = data - 1;
      setData(value);
      onChange(value);
    }
  };

  const handleChange = (e) => {
    setData(Number(e.target.value));
    onChange(Number(e.target.value));
  }

  return (
    <Container>
      <Button onClick={decrease} color="danger">
        -
      </Button>
      <Input type="text" onChange={handleChange} value={data} />
      <Button onClick={increase} color="success">
        +
      </Button>
    </Container>
  );
};

export default Incrementer;
