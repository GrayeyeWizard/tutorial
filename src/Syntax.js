// // // // const x = 25; 
// // // // const y = 5;
// // // // x = 10;
// // // // console.log(x);
// // // //operator

// // // const x = 26;
// // // const y = 5;
// // // const z = '66';
// // // const fname = 'Piyanut';
// // // const lname = 'Nala';
// // // const a1 = x + y;
// // // const a2 = x - y;
// // // const a3 = x * y;
// // // const a4 = x / y;
// // // const a5 = x % y; //mod durole
// // // const a6 = String(y) + z;
// // // const a7 = y + Number(z);
// // // const a8 = fname + ' ' + lname;
// // // const a9 = `${fname} ${lname}.`;

// // // console.log(a1);
// // // console.log(a2);
// // // console.log(a3);
// // // console.log(a4);
// // // console.log(a5);
// // // console.log(a6);
// // // console.log(a7);
// // // console.log(a8);
// // // console.log(a9);

// // // const a = 13;
// // // const b = 6;
// // // const c = '6';
// // // const b1 = a > b;
// // // const b2 = a >= b;
// // // const b3 = a < b;
// // // const b4 = a <= b;
// // // const b5 = a == b;
// // // const b6 = a != b;
// // // const b7 = b == c;
// // // const b8 = b === c;

// // // console.log(b1);
// // // console.log(b2);
// // // console.log(b3);
// // // console.log(b4);
// // // console.log(b5);
// // // console.log(b6);
// // // console.log(b7);
// // // console.log(b8);

// // // const p = true;
// // // const q = false;
// // // const c1 = p && q;
// // // const c2 = p || q;
// // // const c3 = p || q && p;

// // // console.log(c1);
// // // console.log(c2);
// // // console.log(c3);

// // // const d1 = 1 + 2 * (4 / 2) - 3;

// // // console.log(d1);

// // const score = 50;
// // const extra = 10;
// // let grade = '';
// // if (score > 79 || extra === 10) {
// //     grade = 'A';
// // }

// // else if (score > 69) {
// //     grade = 'B';
// // }

// // else if (score > 59) {
// //     grade = 'C';
// // }

// // else if (score > 49) {
// //     grade = 'D';
// // }

// // else {
// //     grade = 'F';
// // }

// // console.log(grade);

// // let words = '';
// // switch (grade) {
// //     case 'A':
// //         words = 'Ant';
// //         break;
// //     case 'B':
// //         words = 'Bird';
// //         break;
// //     case 'C':
// //         words = 'Cat';
// //         break;
// //     case 'D':
// //         words = 'Dog';
// //         break;
// //     case 'F':
// //         words = 'Fish';
// //         break;
// //     default:
// //         words ='Unknown';
// // }

// // console.log(words);

// let id = '....';
// let pw = '....';
// let warn = '';

// if (id === '' && pw === '') {
//     warn = 'Please put your id and password';
// }

// else if (id === '') {
//     warn = 'Please put your id';
// }

// else if (pw === '') {
//     warn = 'Please put your password';
// }

// else {
//     warn = 'your login is success';
// }

// console.log(warn);

let n = 10;

for (let i = 1; i < 15; i = i + 1) {
    if (i == 10){
        continue;
    }
    if (i == 13){
        break;
    }
    console.log(`${n} * ${i} = ${n*i}`);
}

let count = 2;

while (count < 65536){
    count = count * count;
    console.log(count);
}

count = 65536;

do {
    count = count * count;
    console.log(count);
}
while(count < 65536);

const inter = [2,4,6,8,10];
for (let i = 0; i<inter.length; i++){
    console.log(i, inter[i]);
}

const inter2 = inter.map((item,index) => {
    console.log(index,item);
    return item * 2;
});

console.log(inter2);

const profile = [
    {
        fname: 'Piyanut',
        lname: 'Nala',
        dob: {
            date: 9,
            month: 7,
            year: 1992,
        },
        fcolor: ['blue', 'red']
    },
    {
        fname: 'Nut',
        lname: 'La',
        dob: {
            date: 13,
            month: 6,
            year: 1990,
        },
        fcolor: ['gray', 'black']
    }
];

for (let i = 0; i<profile.length; i++){
    console.log(profile[i].fname, `${profile[i].dob.date}/${profile[i].dob.month}/${profile[i].dob.year}`);
    for (let j = 0; j < profile[i].fcolor.length; j++){
    console.log(profile[i].fcolor[j])
    }
}

profile.map((item) => {
    console.log(item.fname, `${item.dob.date}/${item.dob.month}/${item.dob.year}`);
    item.fcolor.map((item2) => {
        console.log(item2);
    });
});

const calVat = (money = 0) => {
    const price = money + (money * 7 / 100);
    return price;
}
const money = 100;
const price = calVat(money);
console.log(price);

const calTri = (height = 0, base = 0) => {
    const area = height * base * 0.5;
    return area;
}
const height = 6;
const base = 13;
const area = calTri(height, base);
console.log(area);

const calSquare = ({height = 0, width}) => {
    const area = height * width;
    return area;
}

const property = {
    width: 9,
    height: 8,
};
const area2 = calSquare(property);
console.log(area2);