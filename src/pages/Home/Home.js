import React, { useState } from "react";
import Incrementer from "../../components/Incrementer/Incrementer";

const Home = () => {
    const [fdw, setFdw] = useState(0);
  return (
    <div>
      <Incrementer onChange={setFdw} value={1}/>
      <Incrementer value={2}/>
      <Incrementer value={3}/>
      <p>{fdw}</p>
    </div>
  );
};

export default Home;
