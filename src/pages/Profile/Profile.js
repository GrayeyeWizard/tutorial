import React, { useState, useEffect} from "react";
import Avata from "../../components/Avata/Avata";
import axios from "axios";

const Profile = () => {
    const [users, setUsers] = useState([]);

    const fetchUser = async () => {
        const response = await axios.get('https://randomuser.me/api/?results=10');
        if (response.status === 200){
            console.log('response', response.data.results);
            setUsers(response.data.results);
        }
    }
    useEffect(() => {
        fetchUser();
        for (let i = 0; i < 10; i ++) {
            console.log('num: ', i + 1);
        }
      }, []);

  return (
    <div>
      <p>Profile</p>
      <Avata/>
      <Avata/>
      {users.map((item, index) => {
          return <p key={index}>{index + 1}. {item.name.first}</p>
      })}
    </div>
  );
};

export default Profile;
